import { transfer } from "./src/transfer";
import { FireblocksSDK} from "fireblocks-sdk";
const fs = require("fs");
const path = require("path");

const apiSecret = fs.readFileSync(path.resolve(""), "utf8");
const apiKey = '';
const fireblocks = new FireblocksSDK(apiSecret, apiKey)

// https://ropsten.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161' for Ropsten
// https://cloudflare-eth.com for Mainnet
const httpProviderURL = '';

//Fireblocks' vault account id of the eth/eth_test wallet that the funds are stuck in
const vaultAccountID = '';

//Make sure that the destination address is on the correct network! 
const destinationAddress = '';

/*The contract address of the token that needs to be recovered, on the original network, for exmaple:
USDC token was sent from ETH mainnet to ETH testnet - put the address of USDC on MAINNET!

Needed only if you are trying to extract a token (and not a base asset)
*/
const contractAddress = null;

//null = the entire stuck amount
const amount = '';

//ETH_TEST for funds that needs to be recovered from Ropsten address, ETH for mainnet
const assetType = '';

transfer(fireblocks, httpProviderURL,vaultAccountID,destinationAddress,assetType,contractAddress, amount).then(res => {
    // You can also use web3.eth.sendSignedTransaction(res) to send this transaction later on.
    console.log(res)
})

