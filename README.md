# Extract unsupported EVM based assets from your Fireblocks ETH address

This scripts allows you to extract unsupported EVM based blockchain funds that were sent to you Fireblocks ETH address

## Getting started

```
git clone https://gitlab.com/fireblocks-solutions-engineerinig/ether-raw-signing.git 

cd ether-raw-signing

npm install
```

## Update the index.ts file

```
REQUIRED: apiSecret - path to your secret key (string)
REQUIRED: apiKey - your Fireblocks API key (string)
REQUIRED: httpsProviderUrl - RPC of the EVM blockchain that the funds were sent on (string)
REQUIRED: vaultAccountId - vault account ID of the ETH wallet that the funds were sent to (string)
REQUIRED: destinationAddress - the destination of the funds: Make sure that the destination address supports the EVM based network (string)
REQUIRED: assetType - ETH for assets that were sent to ETH address, ETH_TEST for Ropsten, ETH_TEST2 for Kovan (string)

OPTIONAL: contract - if the asset that was stuck is an ERC20 like token, provide the contract address of the token on the EVM network (string)
OPTIONAL: amount - the amount to extract. For the ENTIRE amount set to null (string/null)
```

## Important Notes

1. The script requires RAW signing feature enabled in your workspace. For more info - https://support.fireblocks.io/hc/en-us/articles/360017095720-Raw-Signing-API
2. For tokens extraction - you would need to have the base asset of the EVM network (For example, in order to extract Staked ROME tokens on Moonriver, you'll need to send some MOVR to the ETH address to cover the network fees)

## Disclaimer

This script was tested on Fireblocks production wallets, having said that, running RAW signing operations is completely on customer's responsibility. 
Fireblocks is not responsible for any lost of funds that was caused by running these scripts.
Our engineering teams are here to assist with any setup related or execution questions.


