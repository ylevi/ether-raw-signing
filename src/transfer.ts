import { initWeb3Instance }  from "./web3_instance";
import { FireblocksSDK } from "fireblocks-sdk";

export async function transfer(apiClient: FireblocksSDK, httpProviderUrl, vaultAccountId, destAddress, assetId, contractAddress?, amount?) {
    
    
    const web3 = await initWeb3Instance(apiClient, httpProviderUrl, vaultAccountId, assetId);
    const abi = JSON.parse('[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"burn","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getOwner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"name","type":"string"},{"internalType":"string","name":"symbol","type":"string"},{"internalType":"uint8","name":"decimals","type":"uint8"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"bool","name":"mintable","type":"bool"},{"internalType":"address","name":"owner","type":"address"}],"name":"initialize","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"mint","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"mintable","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}]');


    let rawTransaction;

    if (contractAddress) {
        const gasPrice = web3.utils.toBN(await web3.eth.getGasPrice());
        console.log('Gas price', await web3.eth.getGasPrice());
        const gasLimit = 300000;   
        const contract = new web3.eth.Contract(abi, contractAddress);
        console.log(JSON.stringify(contract, null, 2))
        let actualAmount;
        if(amount) {
            const decimals = web3.utils.toBN(await  contract.methods.decimals().call());
            console.log('decimals', decimals);
            actualAmount = web3.utils.toBN(10).pow(web3.utils.toBN(decimals)).mul(web3.utils.toBN(amount));
        } else {
            actualAmount = await contract.methods.balanceOf(web3.eth.defaultAccount).call();    
        }
        console.log(`amount to withdraw is ${actualAmount}`);
        if(actualAmount == 0) {
            return;
        }
        const data = contract.methods.transfer(destAddress, actualAmount).encodeABI({ from: web3.eth.defaultAccount});
        rawTransaction = await web3.eth.signTransaction({ 
            to: contractAddress,
            data,
            value: web3.utils.toBN("0x00"),
            gasPrice,
            gasLimit
        });
        console.log("Fee Token Balance", await web3.eth.getBalance(web3.eth.defaultAccount));
    } else {
        const gasPrice = web3.utils.toBN(await web3.eth.getGasPrice());
        const gasLimit = 21000;
        let value;
        if(amount) {
            value = web3.utils.toWei(amount)
        } else {
            const balance = web3.utils.toBN(await web3.eth.getBalance(web3.eth.defaultAccount));
            const gas = gasPrice.mul(web3.utils.toBN(gasLimit));
            value = balance.sub(gas);
        }
        console.log(`amount to withdraw is ${value}`);
        if(value == 0) {
            return;
        }
        rawTransaction = await web3.eth.signTransaction({ 
            to: destAddress,
            value,
            gasPrice,
            gasLimit
        });    
    }

    // This can be later sent through web3.eth.sendSignedTransaction(rawTransaction)
    return rawTransaction
}